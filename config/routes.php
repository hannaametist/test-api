<?php

return [
    '/'         => 'app\controller\Site',
    '/dogs'     => 'app\controller\Dog',
    '/dogs/:id' => 'app\controller\Dog',
    '/cats'     => 'app\controller\Cat'
];