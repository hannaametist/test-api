<?php
/**
 * Function for loading classes
 *
 * @param $className
 * @return bool
 */
function loadApp($className) {
    $classPath = str_replace('\\', '/', $className) . '.php';
    if (file_exists($classPath)) {
        require_once $classPath;
        return true;
    }
    return false;
}

spl_autoload_register('loadApp');