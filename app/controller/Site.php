<?php

namespace app\controller;

use core\ApiReadInterface;

class Site implements ApiReadInterface
{
    public function index()
    {
        return ['dogs'];
    }
}