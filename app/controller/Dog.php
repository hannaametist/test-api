<?php

namespace app\controller;


use core\ApiCrudInterface;
use core\ApiReadInterface;
use core\Application;

class Dog implements ApiReadInterface, ApiCrudInterface
{
    private static $collection = [
        [
            'id'    => 1,
            'name'  => 'Takso',
            'breed' => 'dachshund',
        ],
        [
            'id'    => 2,
            'name'  => 'Reks',
            'breed' => 'sheepdog',
        ],
        [
            'id'    => 3,
            'name'  => 'Alt',
            'breed' => 'alabai',
        ],
    ];

    public function index()
    {
        return self::$collection;
    }

    public function create()
    {
        // TODO: Implement create() method.
    }

    public function read($id)
    {
        foreach (self::$collection as $key => $collectElm) {
            if ($collectElm['id'] == $id) {
                Application::$app->response->setStatusCode('200');
                return self::$collection[$key];
            }
        }

        throw new \Error('', 404);
    }

    public function update($id)
    {
        // TODO: Implement update() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }
}