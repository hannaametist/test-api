<?php

require_once __DIR__.'/../config/autoload.php';

/**
 * Add array of routes' template
 */
\core\components\Router::addRoute(require __DIR__.'/../config/routes.php');

/**
 * Create instance of the application
 */
$app = new \core\Application(new \core\Config());

return $app;