<?php
/**
 * Create instance of application and setup
 */
$app = require __DIR__.'/app/app.php';

/**
 * Run the application
 */
$app->run();
