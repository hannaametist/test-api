<?php

namespace core\formatters;

use core\components\ResponseInterface;

interface ResponseFormatterInterface
{
    public function format(ResponseInterface $response);
}