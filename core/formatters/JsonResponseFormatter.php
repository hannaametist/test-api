<?php

namespace core\formatters;

use core\components\ResponseInterface;

/**
 * Class JsonResponseFormatter
 *
 * JsonResponseFormatter formats the given data into a JSON content.
 * Used by [[Response]] for formatting response data
 *
 * @package core\formatters
 */
class JsonResponseFormatter implements ResponseFormatterInterface
{
    /**
     * Encoded [[data]] from request to json format
     * Throws \Error if there was errors
     * Set header and content in response
     *
     * @param ResponseInterface $response
     * @throws \Error with code 500 (Internal Server Error) ang message - it's last json error
     */
    public function format(ResponseInterface $response)
    {
        $content = json_encode($response->getData());
        if(!$content) {
            throw new \Error(json_last_error_msg(), 500);
        }

        $response->setHeader('Content-Type', 'application/json; charset=UTF-8');
        $response->setContent($content);
    }
}