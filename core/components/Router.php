<?php

namespace core\components;

use core\ApiCrudInterface;
use core\ApiReadInterface;
use core\Application;

/**
 * Class Router
 * This class handling request.
 * You can use it before running [[Application]] with [[addRoute()]] -
 * in this way Router understand
 * where to send request and which controller and action must be called
 *
 * Is configured as an application component in [[\core\Application]] by default.
 * You can access instance of this component by `Application::$app->router`.
 *
 * Provides interface for handling incoming request
 *
 * @package core\components
 */
class Router extends Component implements RouterInterface
{
    /**
     * Holds list of routes added before [[Application]] runs.
     * Every pair key-value must look like this (for example):
     * `'/' => 'app\controller\Site'
     * where key (`'/'`) - it's requested route and value (`'app\controller\Site'`) - it's controller
     * that must perform request
     *
     * @var array
     */
    protected static $routes = [];

    /**
     * This property contains list of request methods, which used only as readonly.
     * All controllers, that support this methods - must implement [[ApiReadInterface]]
     *
     * @var array
     * @see ApiReadInterface
     */
    protected $readonlyMethods = ['GET'];

    /**
     * This property contains list of request methods, which can change collection or element of collection.
     * All controllers, that support this methods - must implement [[ApiCrudInterface]]
     *
     * @var array
     * @see ApiCrudInterface
     */
    protected $changingMethods = ['POST', 'PUT', 'DELETE'];

    /**
     * Holds map for calling action in selected controller.
     * Key is method and value - action name
     *
     * @var array
     */
    protected $actionMap = [
        'default'   => 'index',
        'GET'       => 'read',
        'POST'      => 'create',
        'PUT'       => 'update',
        'DELETE'    => 'delete',
    ];

    /**
     * Holds patterns which you can use in setting routes
     * Such patterns used in finding route
     *
     * @var array
     * @see findRoute()
     */
    protected $patterns = [
        ':id' => '([0-9]+)',
    ];

    /**
     * Method add new routes to known list of route.
     * You can add one route or array of router.
     *
     *
     * @param string|array $route It may be one route,
     * for example `/dogs`
     * or array of routes, where every key is route and value - path to controller
     * for example
     * `['/' => 'app\controller\Site', '/dogs' => 'app\controller\Dog']`
     *
     * @param null $controller If you add single route - add path to controller with it
     */
    public static function addRoute($route, $controller = null)
    {
        if ($controller != null && !is_array($route)) {
          $route = array($route => $controller);
        }
        self::$routes = array_merge(self::$routes, $route);
    }

    /**
     * Method get uri, find appropriate route and return result executing action.
     *
     * @return mixed
     */
    public function handleRequest()
    {
        $requestedUrl = Application::$app->request->getUri();
        $findedRoute = $this->findRoute($requestedUrl);
        return $this->execute(self::$routes[$findedRoute]);
    }

    /**
     * Method take requestUri, compare it with exists routes (using [[patterns]])
     * and return detected key of route from [[routes]]
     *
     * @param string $requestedUrl
     * @return string
     * @throws \Error with code 404 (Not found) if route was not found
     * @see routes
     * @see patterns
     */
    protected function findRoute(string $requestedUrl)
    {
        if (isset(self::$routes[$requestedUrl])) {
            return $requestedUrl;
        }

        foreach (self::$routes as $route => $controller) {
            foreach ($this->patterns as $alias => $pattern) {
                if (strpos($route, $alias) !== false) {
                    $routeTemplate = str_replace($alias, $pattern, $route);

                    if (preg_match('#^' . $routeTemplate . '$#', $requestedUrl)) {
                        return $route;
                    }
                }
            }
        }

        throw new \Error('', 404);
    }

    /**
     * This gets method and params from request, checking controller (@see checkController)
     * and called appropriated action
     *
     * @param string $controller
     * @return mixed
     */
    protected function execute(string $controller)
    {
        $method = Application::$app->request->getMethod();
        $params = Application::$app->request->getParams();

        $checkingResult = $this->checkController($controller, $method);

        if ($checkingResult) {
            return call_user_func_array(array($controller, $this->getAction($method)), $params);
        }
    }

    /**
     * Method check if requested controller is exist,
     * and supports request-method
     *
     * @param $controller
     * @param $method
     * @return bool
     */
    private function checkController($controller, $method)
    {
        if (!class_exists($controller)) {
            throw new \Error('Controller ' . $controller . ' is not exists.', 500);
        }

        $classObject = new $controller;

        if (in_array($method, $this->readonlyMethods) && !($classObject instanceof ApiReadInterface)) {
            throw new \Error('', 405);
        }

        if (in_array($method, $this->changingMethods) && !($classObject instanceof ApiCrudInterface)) {
            throw new \Error('', 405);
        }

        return true;
    }

    /**
     * Method return action, which was called in controller.
     * If request has no params - returns default action
     *
     * @param string $method
     * @param array $params
     * @return string
     */
    public function getAction(string $method = '', array $params = []) : string
    {
        $method = $method ?: Application::$app->request->getMethod();
        $params = $params ?: Application::$app->request->getParams();

        if (count($params) === 0) {
            return $this->actionMap['default'];
        } else {
            return $this->actionMap[$method];
        }
    }
}