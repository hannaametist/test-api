<?php

namespace core\components;


/**
 * Class Request
 * This class represents request handled by application.
 * Encapsulates the $_SERVER variables.
 *
 * Is configured as an application component in [[\core\Application]] by default.
 * You can access instance of this component by `Application::$app->request`.
 *
 * Provides interface for getting request method, headers, requested uri and accepted types of content.
 *
 * @package core\components
 */
class Request extends Component implements RequestInterface
{
    /**
     * Holds request method from $_SERVER
     *
     * @var string
     */
    protected $method;

    /**
     * Holds parsed request uri, without GET-parameters
     *
     * @var string
     */
    protected $requestUri;

    /**
     * Holds handled headers' pair key-value
     *
     * @var array
     */
    protected $headers = [];

    /**
     * Holds parameters (requested single elements) from uri.
     * Every even part from uri is considered as $id of single element of requested collection
     *
     * @var array
     */
    protected $params = [];

    /**
     * Methods, which is allowed for this application
     *
     * @var array
     */
    protected $allowedMethods = [
        'GET',
        'POST',
        'PUT',
        'DELETE',
    ];

    /**
     * Keep array of content types from header Accept.
     * Has default value, which is used where header Accept isn't exist
     *
     * @var array
     */
    protected $acceptContentTypes = ['*/*'];

    /**
     * Array of content types, which is allowed for this application
     *
     * @var array
     */
    private $allowedAccept = [
        '*/*',
        'application/json',
    ];

    /**
     * Fill new instance of Request.
     *
     * @return void
     */
    public function init()
    {
        $this->setMethod();
        $this->setHeaders();
        $this->setAcceptContentType();
        $this->setUri();
        $this->setParams();
    }

    /**
     * Return the method of current request, which is stored in property
     * @return string
     */
    public function getMethod() : string
    {
        return $this->method;
    }

    /**
     * Set the method of current request in property
     *
     * @throws \Error If method isn't allowed - `405 Not Acceptable`
     * @return void
     */
    protected function setMethod()
    {
        if ($this->isAllowedMethod($this->parseMethod())) {
            $this->method = $this->parseMethod();
        } else {
            throw new \Error('', 405);
        }
    }

    /**
     * Take requested method from headers.
     * Method GET is an default
     *
     * @return string
     */
    protected function parseMethod() : string
    {
        return strtoupper(
            $_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE']
            ?? $_SERVER['REQUEST_METHOD']
            ?? 'GET'
        );
    }

    /**
     * Return requested uri of current request (take from property)
     *
     * @return string
     */
    public function getUri() : string
    {
        return $this->requestUri;
    }

    /**
     * Set parsed uri to property
     *
     * @return void
     */
    protected function setUri()
    {
        $this->requestUri = $this->parseUri();
    }

    /**
     * Parse current requested uri.
     * Crop GET-parameters.
     * If request uri is not '/' - crop first slash.
     *
     * @return string
     */
    private function parseUri() : string
    {
        $requestUri = strlen($_SERVER['REQUEST_URI']) > 1
            ? rtrim($_SERVER['REQUEST_URI'], '/')
            : $_SERVER['REQUEST_URI'];

        return reset(explode('?', $requestUri));
    }

    /**
     * Return array of headers, which is stored in property of such class
     *
     * @return array
     */
    public function getHeaders() : array
    {
        return $this->headers;
    }

    /**
     * Set parsed headers to such class property
     *
     * @return void
     */
    protected function setHeaders()
    {
        $this->headers = $this->parseHeaders();
    }

    /**
     * Return array of handled headers
     *
     * @return array
     * @throws \Error with code 400 (Bad request), if current request has no headers
     * @throws \Error with code 401 (Unauthorized ), if current request has no Authorization header
     */
    private function parseHeaders() : array
    {
        $headers = [];
        $preparedHeaders = [];
        if (function_exists('getallheaders')) {
            $headers = getallheaders();
        }

        if (function_exists('http_get_request_headers')) {
            $headers = http_get_request_headers();
        }

        if (empty($headers)) {
            $headers = $_SERVER;
            foreach ($headers as $headerName => $headerValue) {
                if (strpos($headerName, 'HTTP_') === 0) {
                    $headerName = str_replace(' ', '_', ucwords(substr($headerName, 5)));
                    $preparedHeaders[$headerName] = $headerValue;
                }
            }

            $headers = $preparedHeaders;
        }

        if (empty($headers)) {
            throw new \Error('Headers list is empty', 400);
        }

        if (!isset($headers['AUTHORIZATION'])) {
            throw new \Error('', 401);
        }

        return $headers;
    }

    /**
     * Return array of keys of requested single elements (stored in property)
     *
     * @return array
     */
    public function getParams() : array
    {
        return $this->params;
    }

    /**
     * Set parsed parameter to property
     *
     * @return void
     */
    protected function setParams()
    {
        $this->params = $this->parseParams();
    }

    /**
     * Parse params from requested uri.
     * Parameters are requested keys of collections elements.
     * For example:
     * Uri - `clients/56` - `56` - It's key of single element from Clients collection.
     * Or it is parameter for Client collection.
     *
     * @return array
     */
    private function parseParams() : array
    {
        $urlParts = explode('/', ltrim($this->requestUri, '/'));

        $countParts = count($urlParts);

        $params = [];

        for ($key = 0; $key < $countParts; $key++) {
            if(($key % 2)){
                $params[] = $urlParts[$key];
            }
        }

        return $params;
    }

    /**
     * Return array of accepted types of content, stored in property
     *
     * @return array
     */
    public function getAcceptContentTypes() : array
    {
        return $this->acceptContentTypes;
    }

    /**
     * Set parsed types of accepted content if header Accept is exists
     *
     * @return void
     */
    private function setAcceptContentType()
    {
        if (isset($this->headers['ACCEPT'])) {
            $this->acceptContentTypes = $this->parseAcceptHeader();
        }
    }

    /**
     * Parse header Accept.
     * Every accepted content type is compared with lists of allowed types of content
     * selects only coinciding types.
     *
     * @return array
     * @throws \Error with code 406 (Not Acceptable) if header no stored allowed types of content
     */
    private function parseAcceptHeader()
    {
        $accepts = [];

        foreach (explode(',', $this->headers['ACCEPT']) as $part) {
            $accept = trim(reset(explode(';', $part)));

            if (in_array($accept, $this->allowedAccept)) {
                $accepts[] = $accept;
            }
        }

        if (!empty($accepts)) {
            return $accepts;
        }

        throw new \Error('', 406);
    }

    /**
     * Check if method exists in list of allowed methods of application
     *
     * @param string $method
     * @return bool
     */
    protected function isAllowedMethod(string $method) : bool
    {
        return in_array($method, $this->allowedMethods);
    }
}