<?php

namespace core\components;


/**
 * Class Component
 * Base class for all components in application.
 * New components need extend it or implement ComponentInterface
 * @package core\components
 */
class Component implements ComponentInterface
{
    /**
     * This method is called after constructing component when it's called as Application property
     */
    public function init()
    {
    }
}