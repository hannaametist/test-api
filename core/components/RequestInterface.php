<?php

namespace core\components;


interface RequestInterface
{
    /**
     * Return methods of current request
     *
     * @return string
     */
    public function getMethod() : string;

    /**
     * Return current uri
     *
     * @return string
     */
    public function getUri() : string;

    /**
     * Return lists of current headers
     *
     * @return array
     */
    public function getHeaders() : array;

    /**
     * Return lists of requested parameters
     *
     * @return array
     */
    public function getParams() : array;

    /**
     * Return list of content types from Accept header
     *
     * @return array
     */
    public function getAcceptContentTypes() : array;
}