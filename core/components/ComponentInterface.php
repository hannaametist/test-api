<?php

namespace core\components;


/**
 * Interface ComponentInterface
 * @package core\components
 */
interface ComponentInterface
{
    /**
     * Init method for each component in application
     */
    public function init();
}