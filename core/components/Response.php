<?php

namespace core\components;

use core\Application;
use core\formatters\ResponseFormatterInterface;

/**
 * Class Response
 * This class represents the response to request.
 * It has the [[headers]], [[content]], [[statusCode]], [[statusText]] which will be sent to the client.
 * Also it holds [[format]], in which response will be transformed
 *
 * Is configured as an application component in [[\core\Application]] by default.
 * You can access instance of this component by `Application::$app->response`.
 *
 * Provides interface for getting unprepared data (result of handling request), status code, headers,
 * setting status code, header, content and sending response.
 *
 * This class also has list of default formatters for sending content,
 * which format will be used - it depends on request - existence Accept header,
 * which format can read client and existence needle format in default list.
 *
 * @package core\components
 */
class Response extends Component implements ResponseInterface
{
    const FORMAT_HTML = 'html';
    const FORMAT_JSON = 'json';
    const FORMAT_XML = 'xml';

    /**
     * Hold status text
     *
     * @var string
     */
    public $statusText = 'OK';

    /**
     * Hold default format
     *
     * @var string
     */
    public $defaultFormat = self::FORMAT_JSON;

    /**
     * Holds format.
     * Explains how convert [[data]] into [[content]].
     * Supports formats:
     * [[FORMAT_JSON]] - data will be transformed to json
     * and header (Content-Type) will be set as `application/json`
     *
     * @var string
     * @see defaultFormatters()
     */
    private $format;

    /**
     * Holds pairs key-value, where key is status code and value - status text
     *
     * @var array
     */
    private static $statusMessageMap = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-status',
        208 => 'Already Reported',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Switch Proxy',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Large',
        415 => 'Unsupported Media Type',
        416 => 'Requested range not satisfiable',
        417 => 'Expectation Failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable Entity',
        423 => 'Locked',
        424 => 'Failed Dependency',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        451 => 'Unavailable For Legal Reasons',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported',
        506 => 'Variant Also Negotiates',
        507 => 'Insufficient Storage',
        508 => 'Loop Detected',
        511 => 'Network Authentication Required',
    ];

    /**
     * Default is 200
     *
     * @var int the HTTP status code to send with the response.
     */
    private $statusCode = 200;

    /**
     * Holds prepared content, which is ready to sending
     *
     * @var
     */
    private $content;

    /**
     * Holds unprepared content - result of handling request
     * If \Error throws,this property contain message of error
     *
     * @var array
     * @see setData()
     */
    private $data;

    /**
     * Holds list of headers for sending to client
     *
     * @var array
     */
    private $headers;

    /**
     * Method checked data and set it to property,
     * select formatter for formatting response and format data with it,
     * send headers and content to client
     *
     * @param mixed It may be array, string, instance of \Error
     * @return void
     */
    public function send($data)
    {
            $this->setData($data);
            $this->prepareContent($this->selectFormatter(Application::$app->request));
            $this->sendHeaders();
            $this->sendContent();
    }

    /**
     * Return status code
     *
     * @return int
     */
    public function getStatusCode() : int
    {
        return $this->statusCode;
    }

    /**
     * Set status code and status text for sending to client
     *
     * @param $value int - this will be checked for validity
     * @param null $text if text is null, but it is exist in [[statusMessageMap]] - text will be taken from [[statusMessageMap]]
     * in other way text will be empty
     * @return void
     * @throws \Error if code is invalid
     * @see statusMessageMap
     * @see isCodeInvalid()
     */
    public function setStatusCode($value, $text = null)
    {
        if ($value == null) {
            $value = 200;
        }
        $this->statusCode = (int) $value;
        if ($this->isCodeInvalid()) {
            throw new \Error("The HTTP status code is invalid: $value", 500);
        }
        if ($text == null) {
            $this->statusText = isset(static::$statusMessageMap[$this->statusCode])
                ? static::$statusMessageMap[$this->statusCode]
                : '';
        } else {
            $this->statusText = $text;
        }
    }

    /**
     * @return bool whether this response has a valid [[statusCode]].
     */
    public function isCodeInvalid() : bool
    {
        return $this->getStatusCode() < 100 || $this->getStatusCode() >= 600;
    }

    /**
     * Method allow add header to list of headers
     *
     * @param $name
     * @param string $value
     * @return void
     */
    public function setHeader($name, $value = '')
    {
        $name = strtolower($name);
        $this->headers[$name] = (array) $value;
    }

    /**
     * Returns list of headers
     *
     * @return array
     */
    public function getHeaders() : array
    {
        return $this->headers;
    }

    /**
     * Return unprepared content - result of handling request
     *
     * @return array
     */
    public function getData() : array
    {
        return $this->data;
    }

    /**
     * If handling request doesn't throw \Error - set result as array to [[data]]
     * In another way it set \Error message to data
     *
     * @param mixed $data This may be array, string, instance of \Error
     */
    public function setData($data)
    {
        if ($data instanceof \Error) {
            $this->setStatusCode($data->getCode());
            $data = !empty($data->getMessage()) ? $data->getMessage() : [];
        }
        $this->data = (array) $data;
    }

    /**
     * Set prepared data to [[content]]
     *
     * @param array $content
     * @see prepareContent()
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Method uses selected formatter for formatting [[data]]
     *
     * @param ResponseFormatterInterface $formatter
     * @return void
     */
    protected function prepareContent(ResponseFormatterInterface $formatter)
    {
        $formatter->format($this);
    }

    /**
     * Set format
     *
     * @param $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * Method returns instance of response formatter if it exists or null
     *
     * @param $format
     * @return null|ResponseFormatterInterface
     * @see defaultFormatters()
     */
    protected function getFormatter($format)
    {
        $defaultFormatters = $this->defaultFormatters();
        return new $defaultFormatters[$format] ?? null;
    }

    /**
     * Methods see which types in content client requested and try to find correct formatter.
     *
     * Default format is used when:
     *  - client accept any format;
     *  - there was throws \Error and [[Application]] must notify client about it;
     *
     * @param RequestInterface $request
     * @return ResponseFormatterInterface
     * @throws \Error when [[Application]] hasn't applicable format
     * @see getFormatter()
     */
    protected function selectFormatter(RequestInterface $request) : ResponseFormatterInterface
    {
        if ($this->format) {
            return $this->getFormatter($this->format);
        }

        $acceptContentTypes = $request->getAcceptContentTypes();

        if (reset($acceptContentTypes) === '*/*') {
            return $this->getFormatter($this->defaultFormat);
        }

        foreach ($acceptContentTypes as $acceptContentType) {
            $format = end(explode('/', $acceptContentType));

            if ($this->hasFormatter($format)) {
                return $this->getFormatter($format);
            }
        }

        $this->setFormat($this->defaultFormat);

        throw new \Error("Unsupported response format: {$format}", 500);
    }

    /**
     * Returns list of pairs: format - its class
     *
     * @return array
     */
    protected function defaultFormatters() : array
    {
        return [
            self::FORMAT_JSON => 'core\formatters\JsonResponseFormatter',
        ];
    }

    /**
     * Checks if needle format exists in list of default formatters
     *
     * @param $format
     * @return bool
     * @see defaultFormatters()
     */
    protected function hasFormatter($format) : bool
    {
        return key_exists($format, $this->defaultFormatters());
    }

    /**
     * Send [[headers]] to client
     */
    protected function sendHeaders()
    {
        if (headers_sent()) {
            return;
        }

        if ($this->headers) {
            foreach ($this->headers as $name => $values) {
                $name = str_replace(' ', '-', ucwords(str_replace('-', ' ', $name)));
                $replace = true;
                foreach ($values as $value) {
                    header("$name: $value", $replace);
                    $replace = false;
                }
            }
        }

        header("HTTP/1.1 {$this->getStatusCode()} {$this->statusText}");
    }

    /**
     * Send [[content]] to client
     */
    protected function sendContent()
    {
        echo $this->content;
    }
}