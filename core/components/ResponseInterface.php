<?php

namespace core\components;


interface ResponseInterface
{
    public function send($data);
    public function getData();
    public function getStatusCode();
    public function setStatusCode($code, $text = null);
    public function getHeaders();
    public function setHeader($name, $value);
    public function setContent($content);
}