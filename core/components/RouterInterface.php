<?php

namespace core\components;


interface RouterInterface
{
    public function handleRequest();
}