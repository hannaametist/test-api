<?php

namespace core;


interface ConfigInterface
{
    public function getComponents() : array;
}