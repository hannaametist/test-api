<?php
/**
 * Created by PhpStorm.
 * User: annakolotushko
 * Date: 26.03.2018
 * Time: 08:28
 */

namespace core;

/**
 * Class Application
 * @package core
 *
 * @property \core\components\Request $request
 * @property \core\components\Router $router
 * @property \core\components\Response $response
 */
class Application
{
    /**
     * @var \core\Application application instance
     */
    public static $app;

    /**
     * @var array list of components that should be run during the application
     */
    private $components = [];

    /**
     * @var array list of components, which were constructed
     */
    private $definedComponents = [];


    /**
     * Application constructor.
     * @param ConfigInterface $config
     */
    public function __construct(ConfigInterface $config)
    {
        if (self::$app != null) {
            return self::$app;
        }

        Application::$app = $this;

        $this->setComponents($this->mergeComponents($config->getComponents()));

        $this->configErrorHandler();
    }

    /**
     * Runs the application
     * Send result of request processing
     *
     * @return void
     */
    public function run()
    {
        $this->response->send($this->router->handleRequest());
    }

    /**
     * @param array $components
     */
    protected function setComponents(array $components)
    {
        $this->components = $components;
    }

    /**
     * Merge defaults (core) components with additional components
     *
     * @param array $components
     * @return array
     */
    private function mergeComponents(array $components) :array
    {
        foreach ($this->coreComponents() as $key => $coreComponent) {
            $components[$key] = $components[$key] ?? $coreComponent;
        }

        return $components;
    }

    /**
     * Returns the list of core application components.
     * @return array
     */
    public function coreComponents() : array
    {
        return [
            'request' => 'core\components\Request',
            'router' => 'core\components\Router',
            'response' => 'core\components\Response',
        ];
    }

    /**
     * Magic method getter
     * Is overridden for accessing components/properties
     *
     * @param string $name
     * @return mixed
     * @throws \Error if non
     */
    public function __get(string $name)
    {
        $getter = 'get' . $name;
        if (method_exists($this, $getter)) {
            return $this->$getter();
        } elseif($this->has($name)) {
            return $this->get($name);
        } else {
            throw new \Error('Getting unknown property: ' . get_class($this) . '::' . $name, 500);
        }
    }

    /**
     * Check if application know about such component
     *
     * @param string $name
     * @return bool
     */
    private function has(string $name) : bool
    {
        return isset($this->components[$name]);
    }

    /**
     * Return the component instance by its id
     * If components' instance is not exist - create new instance and init component when it's called
     *
     * @param string $id component id, for example 'request', 'response'
     * @return \core\components\ComponentInterface|null
     * @throws \Error if component is not found
     */
    public function get(string $id)
    {
        if (isset($this->definedComponents[$id])) {
            return $this->definedComponents[$id];
        }

        if (isset($this->components[$id])) {
            if (!class_exists($this->components[$id])) {
                return null;
            }
            $this->definedComponents[$id] = (new $this->components[$id]);
            $this->definedComponents[$id]->init();
            return $this->definedComponents[$id];
        }

        throw new \Error('Component with such ID: ' . $id . ' is not found', 500);
    }

    /**
     * Return request component
     *
     * @return \core\components\ComponentInterface|null
     * Null returns if request component was removed from core components
     */
    public function getRequest()
    {
        return $this->get('request');
    }

    /**
     * Return router component
     *
     * @return \core\components\ComponentInterface|null
     * Null returns if router component was removed from core components
     */
    public function getRouter()
    {
        return $this->get('router');
    }

    /**
     * Return response component
     *
     * @return \core\components\ComponentInterface|null
     * Null returns if response component was removed from core components
     */
    public function getResponse()
    {
        return $this->get('response');
    }

    /**
     * Set handler for exceptions
     *
     * @return void
     */
    private function configErrorHandler()
    {
        ini_set('display_errors', false);
        set_exception_handler([$this, 'handleException']);
    }

    /**
     * Deactivate error handler for exit from circuit of handling errors
     *
     * @return void
     */
    public function disableErrorHandler()
    {
        restore_exception_handler();
    }

    /**
     * Handles uncaught exceptions\errors.
     * Its was set up as a PHP handler
     *
     * @param \Exception $exception the exception that is not caught
     * @return void
     */
    public function handleException($exception)
    {
        $this->disableErrorHandler();
        Application::$app->response->send($exception);
    }
}
