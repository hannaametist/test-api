<?php

namespace core;

/**
 * Class Config
 * Its experiment, in future it was supposed used for combining all config properties in one Config object
 * @package core
 */
class Config implements ConfigInterface
{
    /**
     * @return array
     */
    public function getComponents() : array
    {
        return [];
    }
}